colors = ['black', 'white']
sizes = ['S', 'M', 'L']
tshirtsOrderedByColor = [(color, size) for color in colors for size in sizes]
print(tshirtsOrderedByColor)

for color in colors:
    for size in sizes:
        print((color, size))

tshirtsOrderedBySize = [(color, size) for size in sizes for color in colors]
print(tshirtsOrderedBySize)